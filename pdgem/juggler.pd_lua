local J = pd.Class:new():register("juggler")

function J:initialize()
  self.currentHand = -1
  self.nextBallID = 0
  self.currentBallCount = 0
  self.targetBallCount = 0
  self.maximumThrow = 0
  self.longestThrow = 0
  self.futureLandings = { }
  self.inlets = 1
  self.outlets = 1
  return true
end

function J:in_1_balls(atoms)
  if type(atoms[1] == "number") then
    self.targetBallCount = math.max(0, math.floor(atoms[1]))
  end
end

function J:in_1_throw(atoms)
  if type(atoms[1] == "number") then
    self.maximumThrow = math.max(0, math.floor(atoms[1]))
    self.longestThrow = math.max(self.longestThrow, self.maximumThrow)
  end
end

function J:in_1_bang()
  -- catch a possible ball
  local caught = self.futureLandings[1]
  -- advance futures
  local n = self.longestThrow
  local i
  for i = 1,n do
    self.futureLandings[i] = self.futureLandings[i + 1]
  end
  -- check what we caught
  if type(caught) == "number" then
    -- a ball
    if self.currentBallCount > self.targetBallCount then
      -- remove it
      self.currentBallCount = self.currentBallCount - 1
      self:outlet(1, "remove", { caught })
    else
      -- collect possible throws without landing collisions
      local choices = { }
      for i = 1,self.maximumThrow do
        if type(self.futureLandings[i]) ~= "number" then
          table.insert(choices, i)
        end
      end
      -- choose a legal throw
      if #choices == 0 then
        self.currentBallCount = self.currentBallCount - 1
        self:outlet(1, "drop", { caught })
      else
        local dice = math.random()
--[[--
        dice = dice * dice
        if self.currentHand < 0 then
          dice = 1 - dice
        end
--]]--
        dice = math.ceil(dice * #choices)
        dice = math.min(math.max(dice, 1), #choices)
        local throw = choices[dice]
        -- check direction of throw
        local direction
        if (throw / 2) == math.floor(throw / 2) then
          direction = 0
        elseif self.currentHand < 0 then
          direction = 1
        else
          direction = -1
        end
        -- schedule future landing
        self.futureLandings[throw] = caught
        self:outlet(1, "throw", { caught, direction, throw })
      end
    end
  else
    -- caught nothing
    if self.currentBallCount < self.targetBallCount then
      -- collect possible throws without landing collisions
      local choices = { }
      for i = 1,self.maximumThrow do
        if type(self.futureLandings[i]) ~= "number" then
          table.insert(choices, i)
        end
      end
      -- choose a legal throw
      if #choices == 0 then
        self:outlet(1, "wait", { })
      else
        local choice = math.random(1, #choices)
        local throw = choices[choice]
        -- check direction of throw
        local direction
        if (throw / 2) == math.floor(throw / 2) then
          direction = 0
        elseif self.currentHand < 0 then
          direction = 1
        else
          direction = -1
        end
        -- introduce a new ball
        local ball = self.nextBallID
        self.nextBallID = self.nextBallID + 1
        self.currentBallCount = self.currentBallCount + 1
        -- schedule future landing        
        self.futureLandings[throw] = ball
        self:outlet(1, "pick", { ball })
        self:outlet(1, "throw", { ball, direction, throw })
      end
    else
      -- no action needed
      self:outlet(1, "pause", { })
    end
  end
  -- swap hands
  self.currentHand = -self.currentHand
end
