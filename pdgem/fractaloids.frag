uniform vec2 roots[8];
uniform vec2 powers[8];
uniform float hues[8];
uniform int activeRoots;
uniform float epsSquared;
uniform sampler2D hatch;

float cmag2(vec2 z) { return dot(z,z); }
vec2 crecip(vec2 z) { float d = cmag2(z); return z / vec2(d, -d); }
vec2 cmul(vec2 a, vec2 b) { return vec2(a.x * b.x - a.y * b.y, a.x * b.y + a.y * b.x); }

vec2 newtonStep(vec2 z) {
  vec2 s = vec2(0.0);
  for (int i = 0; i < 8; ++i) {
    if (i < activeRoots) {
      s += cmul(powers[i], crecip(z - roots[i]));
    }
  }
  return z - crecip(s);
}

vec4 newtonDelta(vec2 z00) {
  vec2 z0 = z00;
  bool done = false;
  int count = -1;
  float root = -1.0;
  float d0 = 1000.0;
  float d1 = 1000.0;
  vec2 z1 = z00;
  for (int n = 0; n < 64; ++n) {
    d1 = d0;
    z0 = newtonStep(z0);
    d0 = 1000.0;
    for (int i = 0; i < 8; ++i) {
      if (i < activeRoots) {
        float d = cmag2(z0 - roots[i].xy);
        if (d < d0) { d0 = d; root = hues[i]; z1 = roots[i]; }
      }
    }
    if (d0 < epsSquared) {
      done = true;
      count = n;
      break;
    }
  }
  float delta = -1.0;
  if (done) {
    delta = float(count);
    if (d0 > 0.0) {
      delta += clamp(log(epsSquared / d1) / log(d0 / d1), 0.0, 1.0);
    }
  }
  return vec4(root, delta, z1);
}

vec3 yuv2rgb(vec3 yuv) {
  return yuv * mat3(1.0, 1.407, 0.0, 1.0, -0.677, -0.236, 1.0, 0.0, 1.848);
}

vec3 splat(vec3 rgb) {
  float m = max(max(max(rgb.x, rgb.y), rgb.z), 1.0);
  return clamp(rgb / m, 0.0, 1.0);
}

void main(void) {
  vec2 z = gl_TexCoord[0].xy;
  vec4 d = newtonDelta(z);
  vec3 c = vec3(0.0);
  if (!(d.x < 0.0 || d.y < 0.0)) {
    vec2 w = (z - d.zw) * 2.0 + vec2(0.5);
    w -= floor(w);
    w /= vec2(4.0, 2.0);
    float p = d.x / 8.0;
    p -= floor(p);
    vec2 k = vec2(2.0 * p, p < 0.5 ? 0.0 : 0.5);
    k -= floor(k);
    float ds = texture2D(hatch, w + k).g;
    float y = pow(clamp(2.0 / (1.0 + pow(d.y, 0.5)) - 2.0/pow(64.0, 0.5), 0.0, 1.0), 0.5);
    float r = clamp(0.2 - 0.05 * ds, 0.0, 1.0) * y;
    float t = d.x * 3.883222077450933;
    c = splat(yuv2rgb(vec3((0.9 + 0.1 * ds) * y, r * cos(t), r * sin(t))));
  }
  gl_FragColor = vec4(c, 1.0);
}
