#!/bin/sh
r=1
n=`openssl rand 1 | hexdump -e "1/1 \"%1u\n\""`
pd -r 48000 -jack -channels 2 -path "${HOME}/opt/lib/pd/extra/Gem" -lib Gem:pdlua -open loader.pd -send "load $n $r"
